# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# This file is the source Rails uses to define your schema when running `bin/rails
# db:schema:load`. When creating a new database, `bin/rails db:schema:load` tends to
# be faster and is potentially less error prone than running all of your
# migrations from scratch. Old migrations may fail to apply correctly if those
# migrations use external dependencies or application code.
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 2021_08_31_151212) do

  create_table "submissions", force: :cascade do |t|
    t.text "limesurvey_id", limit: 4
    t.date "submit_date", null: false
    t.text "last_page", limit: 1, null: false
    t.text "start_language", limit: 2, null: false
    t.text "seed", null: false
    t.date "start_date", null: false
    t.date "date_stamp", null: false
    t.text "ip", limit: 15, null: false
    t.string "ref_url"
    t.text "nom", null: false
    t.text "prenom", null: false
    t.text "matricule", null: false
    t.date "date_naissance", null: false
    t.text "lieu_naissance", null: false
    t.text "contact_1", limit: 12
    t.text "contact_2", limit: 12
    t.text "contact_3"
    t.text "qualite", limit: 2, null: false
    t.text "grade", limit: 3
    t.text "centre_secours", limit: 5, null: false
    t.date "date_faits", null: false
    t.text "commune", limit: 5, null: false
    t.text "commune_autre"
    t.text "adresse_faits"
    t.text "identite_agresseur_1"
    t.text "identite_agresseur_2"
    t.text "circonstances", limit: 2, null: false
    t.string "circonstances_commentaire"
    t.string "description_faits", null: false
    t.text "intervention", limit: 10
    t.text "type_operation", limit: 2
    t.string "type_operation_autre"
    t.boolean "degat_agent", null: false
    t.boolean "niveau_gravite_victime_n1"
    t.boolean "niveau_gravite_victime_n2"
    t.boolean "niveau_gravite_victime_n3"
    t.boolean "consultation_medicale"
    t.boolean "hospitalisation"
    t.text "itt", limit: 4
    t.text "arret_travail", limit: 4
    t.boolean "degat_materiel", null: false
    t.string "materiels_concernes"
    t.boolean "niveau_gravite_materiel_n1"
    t.boolean "niveau_gravite_materiel_n2"
    t.boolean "niveau_gravite_materiel_n3"
    t.boolean "indisponibilite"
    t.text "depot_plainte", limit: 2, null: false
    t.date "date_plainte"
    t.text "signalement_proc", limit: 2
    t.text "lieu_plainte"
    t.boolean "protection_fonctionnelle", null: false
  end

end
