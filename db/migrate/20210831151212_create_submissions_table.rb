class CreateSubmissionsTable < ActiveRecord::Migration[6.1]
  def change
    create_table :submissions do |t|
      t.text        :limesurvey_id,                          limit: 4, unique: true
      t.date        :submit_date,               null: false
      t.text        :last_page,                 null: false, limit: 1
      t.text        :start_language,            null: false, limit: 2
      t.text        :seed,                      null: false
      t.date        :start_date,                null: false
      t.date        :date_stamp,                null: false
      t.text        :ip,                        null: false, limit: 15
      t.string      :ref_url
      t.text        :nom,                       null: false
      t.text        :prenom,                    null: false
      t.text        :matricule,                 null: false
      t.date        :date_naissance,            null: false
      t.text        :lieu_naissance,            null: false
      t.text        :contact_1,                              limit: 12
      t.text        :contact_2,                              limit: 12
      t.text        :contact_3
      t.text        :qualite,                   null: false, limit: 2
      t.text        :grade,                                  limit: 3
      t.text        :centre_secours,            null: false, limit: 5
      t.date        :date_faits,                null: false
      t.text        :commune,                   null: false, limit: 5
      t.text        :commune_autre
      t.text        :adresse_faits
      t.text        :identite_agresseur_1
      t.text        :identite_agresseur_2
      t.text        :circonstances,             null: false, limit: 2
      t.string      :circonstances_commentaire
      t.string      :description_faits,         null: false
      t.text        :intervention,                           limit: 10
      t.text        :type_operation,                         limit: 2
      t.string      :type_operation_autre
      t.boolean     :degat_agent,               null: false
      t.boolean     :niveau_gravite_victime_n1
      t.boolean     :niveau_gravite_victime_n2
      t.boolean     :niveau_gravite_victime_n3
      t.boolean     :consultation_medicale
      t.boolean     :hospitalisation
      t.text        :itt,                                    limit: 4
      t.text        :arret_travail,                          limit: 4
      t.boolean     :degat_materiel,            null: false
      t.string      :materiels_concernes
      t.boolean     :niveau_gravite_materiel_n1
      t.boolean     :niveau_gravite_materiel_n2
      t.boolean     :niveau_gravite_materiel_n3
      t.boolean     :indisponibilite
      t.text        :depot_plainte,             null: false, limit: 2
      t.date        :date_plainte
      t.text        :signalement_proc,                       limit: 2
      t.text        :lieu_plainte
      t.boolean     :protection_fonctionnelle,   null: false
    end
  end
end
