require 'active_record'
require 'net/http'

class Submission < ActiveRecord::Base
  # Hash used to `transform_keys`. Basically snake_cases keys, but also replace
  # somes to more explicit / verbose wording.
  KEYS_MAPPING = {
    id:                            :limesurvey_id,
    submitdate:                    :submit_date,
    lastpage:                      :last_page,
    startlanguage:                 :start_language,
    startdate:                     :start_date,
    datestamp:                     :date_stamp,
    ipaddr:                        :ip,
    refurl:                        :ref_url,
    Nom:                           :nom,
    Prenom:                        :prenom,
    Matricule:                     :matricule,
    DateNaissance:                 :date_naissance,
    LieuNaissance:                 :lieu_naissance,
    'Contact[SQ001]':              :contact_1,
    'Contact[SQ002]':              :contact_2,
    'Contact[SQ003]':              :contact_3,
    Qualite:                       :qualite,
    Grade:                         :grade,
    CentreSecours:                 :centre_secours,
    Date:                          :date_faits,
    Commune:                       :commune,
    'Commune[other]':              :commune_autre,
    AdresseFaits:                  :adresse_faits,
    'IdentiteAgresseur[SQ001]':    :identite_agresseur_1,
    'IdentiteAgresseur[SQ002]':    :identite_agresseur_2,
    Circonstances:                 :circonstances,
    'Circonstances[comment]':      :circonstances_commentaire,
    DescriptionFaits:              :description_faits,
    Intervention:                  :intervention,
    TypeOperation:                 :type_operation,
    'TypeOperation[other]':        :type_operation_autre,
    DegatAgent:                    :degat_agent,
    'niveauGraviteVictime[SQ001]': :niveau_gravite_victime_n1,
    'niveauGraviteVictime[SQ002]': :niveau_gravite_victime_n2,
    'niveauGraviteVictime[SQ003]': :niveau_gravite_victime_n3,
    ConsultationMedicale:          :consultation_medicale,
    Hospitalisation:               :hospitalisation,
    ITT:                           :itt,
    ArretTravail:                  :arret_travail,
    DegatMateriel:                 :degat_materiel,
    MaterielsConcernes:            :materiels_concernes,
    'niveauGravite[SQ001]':        :niveau_gravite_materiel_n1,
    'niveauGravite[SQ003]':        :niveau_gravite_materiel_n2,
    'niveauGravite[SQ006]':        :niveau_gravite_materiel_n3,
    Indisponiblite:                :indisponibilite,
    DepotPlainte:                  :depot_plainte,
    DatePlainte:                   :date_plainte,
    SignalementProc:               :signalement_proc,
    LieuPlainte:                   :lieu_plainte,
    ProtectionFonctionl:           :protection_fonctionnelle
  }.freeze

  def self.all_synchronized
    synchronize && all
  end

  def self.create_from_limesurvey_payload(payload)
    Submission.create! normalize_keys(payload)
  end

  def self.synchronize
    # Ensure all submissions from LimeSurvey are present in the database
    api_responses = LimesurveyService.survey_responses

    db_responses_ids  = Submission.pluck(:limesurvey_id)
    api_responses_ids = api_responses.map { |res| res['id'] }

    missing_responses_ids = api_responses_ids - db_responses_ids

    api_responses.filter_map do |res|
      Submission.create_from_limesurvey_payload(res) if missing_responses_ids.include?(res['id'])
    end
  end

  def self.normalize_keys(hash)
    hash.transform_keys(&:to_sym)
        .transform_keys(Submission::KEYS_MAPPING)
  end

  def self.associate_values(hash)
    hash.transform_values! do |value|
#dont do dis
    end
  end
end
