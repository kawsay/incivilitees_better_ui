# frozen_string_literal: true

class SubmissionPresenter
  def initialize(submission)
    @submission = submission
  end

  def submit_date
    format_date @submission.submit_date
  end

  def date_et_numero_intervention
    html = "<small class='text-muted'>FAITS DU <b>#{self.date_faits}</b>"
    html << "<br>INTERVENTION <b>#{self.intervention}</b></small>" if self.intervention
    html << "</small>"
  end

  def date_faits
    format_date @submission.date_faits
  end
  def identitee
    [self.grade, self.nom, self.prenom, '-', self.matricule].join ' '
  end

  def nom
    @submission.nom.upcase
  end

  def prenom
    @submission.prenom.capitalize
  end

  def qualite
    {
      'A1' => 'SPV',
      'A2' => 'SPP',
      'A3' => 'PATS',
      'A4' => 'Contractuel'
    }[@submission.qualite]
  end

  def grade
    {
      'A1'  => 'Sapeur',
      'A2'  => 'Sapeur de 1ère classe',
      'A3'  => 'Caporal',
      'A4'  => 'Caporal-chef',
      'A5'  => 'Sergent',
      'A6'  => 'Sergent-chef',
      'A7'  => 'Adjudant',
      'A8'  => 'Adjudant-chef',
      'A9'  => 'Lieutenant',
      'A10' => 'Capitaine',
      'A11' => 'Commandant',
      'A12' => 'Lieutenant-colonel',
      'A13' => 'Colonel',
      'A15' => 'Médecin sapeur pompier',
      'A16' => 'Médecin lieutenant',
      'A17' => 'Médecin capitaine',
      'A18' => 'Médecin commandant',
      'A19' => 'Médecin lieutenant-colonel',
      'A20' => 'Médecin colonel',
      'A21' => 'Pharmacien',
      'A22' => 'Pharmacien capitaine',
      'A23' => 'Pharmacien commandant',
      'A24' => 'Infirmier',
      'A25' => 'Infirmier chef',
      'A26' => 'Infirmier principal',
      'A27' => 'Vétérinaire sapeur pompier',
      'A28' => 'Vétérinaire capitaine',
      'A29' => 'Vétérinaire commandant',
      'A30' => 'Vétérinaire lieutenant-colonel'
    }[@submission.grade]
  end

  def centre_secours
		{
			'CS01' => "Aiserey",
			'CS02' => "Aisey-sur-Seine",
			'CS03' => "Arc-sur-Tille / Remilly-sur-Tille",
			'CS04' => "Arnay-le-Duc",
			'CS05' => "Athée",
			'CS06' => "Auxonne",
			'CS07' => "Baigneux-les-Juifs",
			'CS08' => "Beaune",
			'CS09' => "Blaisy-Bas",
			'CS10' => "Bligny-sur-Ouche",
			'CS11' => "Brazey-en-Plaine",
			'CS12' => "Bretigny",
			'CS13' => "Broin/Bonnencontre",
			'CS14' => "Chanceaux",
			'CS15' => "Chatillon-sur-Seine",
			'CS16' => "Corberon / Corgengoux",
			'CS17' => "Darcey",
			'CS18' => "Dijon Est",
			'CS19' => "Dijon Nord",
			'CS20' => "Dijon Transvaal",
			'CS21' => "Fontaine-Francaise",
			'CS22' => "Genlis",
			'CS23' => "Gevrey-Chambertin",
			'CS24' => "Grancey-le-Chateau",
			'CS25' => "Aignay-le-Duc",
			'CS26' => "Is-sur-Tille",
			'CS27' => "Jailly-les-moulins / Villy-en-Auxois",
			'CS28' => "Lacanche",
			'CS29' => "Laignes",
			'CS30' => "Lamarche-sur-Saone",
			'CS31' => "Les deux côtes",
			'CS32' => "Liernais",
			'CS33' => "Losne",
			'CS34' => "Les-Maillys",
			'CS35' => "Marsannay-le-Bois",
			'CS36' => "Merceuil",
			'CS37' => "Meursault",
			'CS38' => "Mirebeau-sur-Bèze",
			'CS39' => "Montbard",
			'CS40' => "Montigny-sur-Aube",
			'CS41' => "Morey-Saint-Denis / Chambolle-Musigny",
			'CS42' => "Nolay",
			'CS43' => "Nuits-Saint-Georges",
			'CS44' => "Pagny-le-Château / Pagny-la-ville",
			'CS45' => "Perrigny-sur-l'Ognon",
			'CS46' => "Pontailler-sur-Saône",
			'CS47' => "Pouilly-en-Auxois",
			'CS48' => "Precy-sous-Thil",
			'CS49' => "Quincey",
			'CS50' => "Recey-sur-Ource / Leuglay-Voulaines",
			'CS51' => "Rouvray",
			'CS52' => "Ruffey-les-Beaune",
			'CS53' => "Saint-Jean-de-Losne",
			'CS54' => "Saint-Julien",
			'CS55' => "Saint-Seine-l'abbaye",
			'CS56' => "Saint-Seine-sur-Vingeanne",
			'CS57' => "Santenay",
			'CS58' => "Saulieu",
			'CS59' => "Saulon-la-chapelle",
			'CS60' => "Selongey",
			'CS61' => "Semur-en-Auxois",
			'CS62' => "Seurre",
			'CS63' => "Sombernon",
			'CS64' => "Thoisy-la-Berchère",
			'CS65' => "Thury",
			'CS66' => "Til-Châtel",
			'CS67' => "Toutry",
			'CS68' => "Trouhans",
			'CS69' => "Val d'Ouche",
			'CS70' => "Venarey-les-Laumes",
			'CS71' => "Vitteaux"
		}[@submission.centre_secours]
  end

  def le_a
    [
      'Le<b>',
      format_date(@submission.date_faits),
      '</b>à<b>',
      @submission.commune,
      '</b><br><small>',
      "(#{@submission.adresse_faits})</small>"
    ].join ' '
  end

  def context
    if @submission.circonstances == 'A1' # Sur intervention
      return [
        circonstances,
        "(<b>#{type_operation}</b> n°<b>#{@submission.intervention}</b>)"
      ].join ' '
    else
      circonstances
    end
  end

  def identite_agresseur
    if @submission.identite_agresseur_1 || @submission.identite_agresseur_2
      return [
        @submission.identite_agresseur_1,
        @submission.identite_agresseur_2
      ].join ' '
    end
  end

  def violence_agent
    return niveau_gravite if @submission.degat_agent
  end
  def circonstances
		{
      'A1' => 'Sur intervention',
      'A2' => 'En service (hors intervention)'
    }[@submission.circonstances]
	end

  def description_faits
    "<i>#{@submission.description_faits}</i>"
  end

  def type_operation
    {
      'A1' => 'SAP',
      'A2' => 'INC',
      'A3' => 'OD',
      'A4' => 'AC',
      'A5' => 'RTN'
    }[@submission.type_operation]
  end

  def degat_agent
    @submission.degat_agent ? 'Oui' : 'Non'
  end

  def niveau_gravite
    if @submission.niveau_gravite_victime_n1
      return '<div class="alert alert-warning">Agression verbales (insultes, injures, outrages)</div>'
    end

    if @submission.niveau_gravite_victime_n2
      return '<div class="alert alert-warning d-flex align-items-center"><svg class="bi flex-shrink-0 me-2" width="24" height="24" role="img" aria-label="Danger:"><use xlink:href="#exclamation-triangle-fill"/></svg><div>Violence ou tentative d\'agression physique sans armes par destination (crachats, morsure, poing, pied, tête)</div></div>'
    end

    if @submission.niveau_gravite_victime_n3
      return '<div class="alert alert-danger">Violence ou tentative d\'agression physique avec arme par destination (projectile, objet)</div>'
    end

    return '<div class="alert alert-success">Aucun acte de violence renseigné</div>'
  end

  def consultation_medicale
    return '<li>Consultation médicale</li>' if @submission.consultation_medicale
  end

  def plainte
    if @submission.date_plainte
      return "<li>Dépot de plainte effectué ou prévu (le #{format_date(@submission.date_plainte)} à #{@submission.lieu_plainte})</li>"
    else
      return depot_plainte
    end
  end

  def depot_plainte
    {
      'A1' => '<li>Déjà effectué ou prévu</li>',
      'A3' => '<li>Signalement simplifié au Procureur</li>'
    }[@submission.depot_plainte]
  end

  def protection_fonctionnelle
    return '<li>Demande de protection fonctionnelle</li>' if @submission.protection_fonctionnelle
  end

  def signalement_proc
    {
      'A1' => 'Demande de dépot de plainte',
      'A3' => 'Signalement des faits uniquement'
    }[@submission.signalement_proc]
  end

  private

  def format_date(date)
    date.strftime '%d/%m/%Y'
  end

  def method_missing(method_name, *args, &block)
    @submission.send(method_name, *args, &block)
  end
end
