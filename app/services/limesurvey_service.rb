# frozen_string_literal: true

require 'net/http'

class LimesurveyService
  def self.uri
    # Build an URI object
    @uri ||= URI(ENV['LIMESURVEY_API_ENDPOINT'])
  end

  def self.request
    # Build a HTTP POST request & set URI and Content-Type
    @request ||= Net::HTTP::Post.new(uri, 'Content-Type': 'application/json')
  end

  def self.send_request(req)
    # Send the HTTP request passed as parameter (`req`) to the LimeSurvey instance
    Net::HTTP.start(uri.hostname, uri.port, use_ssl: true) do |http|
      http.request(req)
    end
  end

  def self.get_session_key
    # Call the `get_session_key` method from LimeSurvey API
    # Returns the raw JSON payload
    # https://api.limesurvey.org/classes/remotecontrol_handle.html#method_get_session_key
    request.body = 
      {
        method: 'get_session_key',
        params: 
        [
          ENV['LIMESURVEY_LOGIN'],
          ENV['LIMESURVEY_PASSWORD']
        ],
        id: 1
    }.to_json

    res = send_request(request)

    res.body
  end

  def self.session_key
    # Parse the JSON payload from `get_session_key` and memoize the key
    @key ||= JSON.parse(get_session_key)['result']
  end

  def self.export_responses
    # Call the `export_responses` method from LimeSurvey API
    # Returns the raw JSON payload (surveys are return as Base64 encoded JSON payload)
    # https://api.limesurvey.org/classes/remotecontrol_handle.html#method_export_responses
    request.body = 
      {
        method: 'export_responses',
        params: 
        [ 
          session_key,         # $string SessionKey
          ENV['SURVEY_ID'],    # $int    SurveyID
          'json',              # $string DocumentType
          'fr',                # $string LanguageCode
          'complete'           # $string CompletionStatus 
        ],
        id: 1
    }.to_json

    res = send_request(request)

    res.body
  end

  def self.survey_responses
    # Parse the API 'result' part of the response to :export_responses method,
    # Decode the base 64 payload,
    # Parse its JSON content
    payload = Base64.decode64(
      JSON.parse(export_responses)['result']
    )

    responses = JSON.parse(payload)

    # Map & dig the payload's values
    responses.dig('responses').map! do |res|
      res.values.first
    end
  end
end
