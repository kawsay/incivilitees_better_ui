require 'sinatra/base'

module Sinatra
  module HtmlFormater
    def present_data(libelle, data)
      "<div class='row'><div class='col-3'>#{libelle}</div><div class='col'>#{data}</div></div>" if data
    end
  end
end
