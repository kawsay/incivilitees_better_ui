require 'pry'
require 'sinatra'
require 'sinatra/reloader' if development?
require 'sinatra/activerecord'
require 'dotenv/load'
require_relative 'config/environment'
require_relative 'app/models/submission'
require_relative 'app/services/limesurvey_service'
require_relative 'app/presenters/submission_presenter'
require_relative 'app/helpers/html_formater'


# Ensure environment variables are loaded
unless ENV.include? 'LIMESURVEY_LOGIN'
  Dotenv.load File.join(root, '.env')
end

class Incivilitees < Sinatra::Base
  configure :development do
    register Sinatra::Reloader
    helpers Sinatra::HtmlFormater
    set :root,     File.dirname(__FILE__)
    set :views,    Proc.new { File.join(root, 'app', 'views') }
    set :database, 'sqlite3:db/incivilitees.sqlite3'
    # set :bind,     Proc.new { development? ? 'localhost' : '0.0.0.0' }
  end

  set :bind, '0.0.0.0'

  get '/' do
    @submissions = Submission.all_synchronized
    erb :submissions
  end

  get '/test' do
    binding.pry
  end
end
